

import logging
import os
import time
import cv2
from PIL import Image as pilImage
import pytesseract


TESSERACT_EXE_PATH = r"\Tesseract-OCR\tesseract.exe"


class DateFinder:

    @staticmethod
    def read_image_opencv(file):
        image = cv2.imread(file)
        return image

    @staticmethod
    def get_expiration_date_from_image(image):
        """
        Gets a pre-processed image and sends to tesseract (configured to use our pretrained "languages") to find a date.
        Uses logical conditions (dd not over 31, mm not over 12 etc.)
        :param image: Pre-processed image
        :return: string that fits date format dd/mm/yy or dd.mm.yy etc.
        """
        current_yy=None
        expiration_date = pytesseract.image_to_string(image, lang="dat+dig+myl", config="--psm 7")

        # if date is in the form of dd/mm/yy,dd-mm-yy or dd.mm.yy (or yyyy):
        separators = ['.', '/', '-', '\\']
        if len(expiration_date) == 8 or len(expiration_date) == 10:
            if expiration_date[2] in separators and expiration_date[5] in separators:
                try:
                    dd = int(expiration_date[0:2])
                    mm = int(expiration_date[3:5])
                    if len(expiration_date) == 8:
                        current_yy = int(time.strftime("%y"))
                        yy = int(expiration_date[6:8])
                    else:
                        current_yy = int(time.strftime("%Y"))
                        yy = int(expiration_date[6:10])
                except ValueError:
                    dd = 33
                    mm = 15
                    yy = 0
                if dd < 32 and mm < 13 and current_yy + 10 > yy >= current_yy:
                    print(f"Found:{expiration_date}")
                    return expiration_date
        return None

    def scan_products_from_webcam(self):
        """
        Opens a feed from a webcam.
        Gets frames from the camera, displays them with a marker for scanning the date and send to processing &
        analyzing.
        :return: Expiration date
        """
        camera_local = 0
        camera_mobile = 1
        vid_capture = cv2.VideoCapture(camera_mobile)
        products_data = []
        logging.basicConfig(level=logging.INFO)
        print("Starting scan. To stop please click q")
        expiration1 = None
        expiration = None
        if vid_capture.isOpened():
            # Calculate size of scanning marker box
            width = vid_capture.get(3)
            height = vid_capture.get(4)
            top_left_x = int(width / 2) - 200
            top_left_y = int(height / 2) - 50
            bottom_right_xy = (top_left_x + 300, top_left_y + 100)
            start_time = time.time()
            while expiration is None:
                ret, im = vid_capture.read()
                cv2.rectangle(im, (top_left_x, top_left_y), bottom_right_xy, (0, 0, 255), 3)
                cropped = im[top_left_y:bottom_right_xy[1], top_left_x:bottom_right_xy[0]]
                cv2.imshow("Scan Date", im)
                cv2.waitKey(1)
                processed = self.process_image(cropped)
                temp = self.get_expiration_date_from_image(processed)
                # Check if same date was found 2 consecutive times - increase correction
                if expiration1 != temp and temp is not None:
                    expiration1 = temp
                elif expiration is None and temp == expiration1:
                    expiration = temp
            logging.info(f"Found date in {time.time() - start_time} seconds")

        cv2.destroyAllWindows()
        vid_capture.release()
        logging.info(f"Finished scanning. Data: {expiration}")
        return products_data

    @staticmethod
    def process_image(image):
        """
        pre-process the image before sending it to tesseract for analysis.
        preprocessing includes blur (noise removal), grayscale & thresholding and upscaling the image for better results
        Also includes dialating and eroding the image (using openCV's morphology function).
        :param image: image to be processed
        :return: image after processing
        """
        im_blurred = cv2.medianBlur(image, 3)
        im_gray = cv2.cvtColor(im_blurred, cv2.COLOR_RGB2GRAY)
        im_t = cv2.adaptiveThreshold(im_gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 9, 8)
        im_t = cv2.resize(im_t, (0, 0), fx=2.5, fy=3)
        circ = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 9))
        dilated = cv2.morphologyEx(im_t, cv2.MORPH_OPEN, circ)
        return dilated

    def run(self):

        pytesseract.pytesseract.tesseract_cmd = TESSERACT_EXE_PATH
        self.scan_products_from_webcam()


if __name__ == "__main__":
    df = DateFinder()
    df.run()
