## Python Date Scanner
### A college assignment in image processing using Python, OpenCV & Tesseract

This simple python app gets frames from a camera device connected to the host. 
The frames are then processed (threshold, blur, dilation, eroding etc.) and 
sent to a Tesseract engine which, using pre-trained data files, tries to recognize
digits and common seperators in an attempt to read an expiration date. 
